from django.views.generic import TemplateView, FormView, ListView, DetailView
from frontend.models import Language, CodeProfile
from frontend.tasks import send_code_via_email
from frontend.forms import AddCodeForm
from django.contrib import messages

class IndexView(ListView):
	"""
	docstring for IndexView
	"""
	template_name = 'index.html'
	model = CodeProfile

	def get_context_data(self, **kwargs):
		context = super(IndexView, self).get_context_data(**kwargs)

		get_all_language = Language.objects.all()
		user = self.request.user
		code_dict = {}
		code_list = []

		if user.pk:
			get_code = CodeProfile.objects.filter(user=user.id)
		else:
			get_code = CodeProfile.objects.filter(is_private=False)

		for data in get_all_language:
			code_dict['name'] = data.language
			if user.pk:
				code_dict['language_count'] = CodeProfile.objects.filter(user=user.id).filter(language=data.id).count()
			else:
				code_dict['language_count'] = CodeProfile.objects.filter(language=data.id).filter(is_private=False).count()
			code_list.append(code_dict.copy())

		context['user_code'] = get_code
		context['languge_result'] = code_list

		return context


class CodeDetailView(DetailView):
	"""
	docstring for CodeDetailView
	"""
	template_name = 'frontend/code-detail.html'
	model = CodeProfile
	slug_field = 'slug_code'
	
	def get_context_data(self, **kwargs):
		context = super(CodeDetailView, self).get_context_data(**kwargs)

		user = self.request.user
		get_all_language = Language.objects.all()

		code_dict = {}
		code_list = []

		for data in get_all_language:
			code_dict['name'] = data.language
			if user.pk:
				code_dict['language_count'] = CodeProfile.objects.filter(user=user.id).filter(language=data.id).count()
			else:
				code_dict['language_count'] = CodeProfile.objects.filter(language=data.id).filter(is_private=False).count()
			code_list.append(code_dict.copy())

		context['languge_result'] = code_list

		context['code_detail'] = CodeProfile.objects.get(slug_code = self.kwargs['slug'])
		return context


class LanguageFilterView(ListView):
	"""
	docstring for LanguageFilterView
	"""
	template_name = 'index.html'
	model = CodeProfile
	
	def get_context_data(self, **kwargs):
		context = super(LanguageFilterView, self).get_context_data(**kwargs)
		
		user = self.request.user
		get_all_language = Language.objects.all()
		language = Language.objects.get(language = self.kwargs['language']).id

		code_dict = {}
		code_list = []

		for data in get_all_language:
			code_dict['name'] = data.language
			if user.pk:
				code_dict['language_count'] = CodeProfile.objects.filter(user=user.id).filter(language=data.id).count()
			else:
				code_dict['language_count'] = CodeProfile.objects.filter(language=data.id).filter(is_private=False).count()
			code_list.append(code_dict.copy())

		context['languge_result'] = code_list
		if user.pk:
			context['user_code'] = CodeProfile.objects.filter(user=user.id).filter(language = language)
		else:
			context['user_code'] = CodeProfile.objects.filter(language = language).filter(is_private=False)
		return context


class CodeEmailSendView(TemplateView):
	"""
	docstring for CodeEmailSend
	"""
	def render_to_json_response(self, context, **response_kwargs):
		data = json.dumps(context)
		response_kwargs['content_type'] = 'application/json'
		return HttpResponse(data, **response_kwargs)

	def get(self, context, **response_kwargs):
		email = self.kwargs['email_field']
		if email != "None":
			EMAIL_REGEX = re.compile(r"[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}")
			if EMAIL_REGEX.match(email):
				profile = CodeProfile.objects.get(slug_code = self.kwargs['slug_code'])
				send_code_via_email.delay(profile, email)
				msg = 'Email successfully sent'
				return self.render_to_json_response({"msg":msg}, status=200)
			else:
				msg = 'Incorrect email address'
				return self.render_to_json_response({"msg":msg}, status=400)
		else:
			msg = 'Please provide email address'
			return self.render_to_json_response({"msg":msg}, status=400)


class AddCodeView(FormView):
	"""
	docstring for AddCodeView
	"""
	template_name = 'frontend/add-new-code.html'
	form_class = AddCodeForm
	success_url = "/"

	def get_context_data(self, **kwargs):
		context = super(AddCodeView, self).get_context_data(**kwargs)
		context['all_language'] = Language.objects.all()
		return context

	def form_valid(self, form):
		form.save()
		msg = 'Your record has been added successfully.'
		messages.success(self.request, msg)
		return self.render_to_response(self.get_context_data(form=form))

	def form_invalid(self, form):
		return self.render_to_response(self.get_context_data(form=form))

	def post(self, request, *args, **kwargs):
		form_class = self.get_form_class()
		form = self.get_form(form_class)
		if form.is_valid():
			return self.form_valid(form)
		else:
			return self.form_invalid(form)