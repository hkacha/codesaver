import hashlib
from django.db import models
from accounts.models import Profile
from django.db.models.signals import post_save, pre_save

class Language(models.Model):
	"""
	This model is used to add new language
	"""
	language = models.CharField(max_length=255, blank=True, null=True)

	def __unicode__(self):
		return self.language

	class Meta:
		verbose_name = "Language"
		verbose_name_plural = "Languages"


class CodeProfile(models.Model):
	"""
	This model is used to store codes of users.
	"""
	user = models.ForeignKey(Profile)
	language = models.ForeignKey(Language)
	title = models.CharField(max_length=255)
	code = models.TextField()
	description = models.TextField(blank=True, null=True)
	is_private = models.BooleanField(default=False)
	date_of_create = models.DateTimeField(auto_now_add=True)
	slug_code = models.SlugField(max_length=200, blank=True, null=True)

	class Meta:
		verbose_name = "Code"
		verbose_name_plural = "Codes"

	def __unicode__(self):
		return "%s_%s_%s" % (self.language, self.title.replace(" ", "_"), self.user.username)

def generate_code_slug(instance, created, **kwargs):
	if created:
		instance.slug_code = '%s-%s-%s' % (hashlib.md5(instance.user.username).hexdigest()[:-20], instance.language, instance.title.replace(" ", "-"))
		instance.save()
post_save.connect(generate_code_slug, sender=CodeProfile)