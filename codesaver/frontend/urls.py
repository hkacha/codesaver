from django.conf.urls import patterns, include, url
from .views import AddCodeView, CodeDetailView, CodeEmailSendView, LanguageFilterView

urlpatterns = patterns('',
						url(
							regex = r'^add/$',
							view = AddCodeView.as_view(),
							name = 'add-new-code'
						),
						url(
							regex = r'^language/(?P<language>[-\w\d]+)/$',
							view = LanguageFilterView.as_view(),
							name = 'language-filter'
						),
						url(
							regex = r'^sendcodeemail/$',
							view = CodeEmailSendView.as_view(),
							name = 'sendcodeemail'
						),
						url(
							regex = r'^(?P<slug>[-\w\d]+)/$',
							view = CodeDetailView.as_view(),
							name = 'code-detail'
						),
					)