from django import forms

from frontend.models import CodeProfile

class AddCodeForm(forms.ModelForm):
	"""
	Add new code form
	"""

	class Meta:
		model = CodeProfile
		fields = ('user','language','title', 'code', 'description')

	def save(self):
		instance = super(AddCodeForm, self).save()
		return instance.save()

	def clean(self):
		cleaned_data = super(AddCodeForm, self).clean()

		user = cleaned_data.get('user')
		language = cleaned_data.get('language')
		title = cleaned_data.get('title')
		code = cleaned_data.get('code')
		description = cleaned_data.get('description')

		if not language:
			msg = "This field is required."
			self._errors['language'] = self.error_class([msg])

		if not title:
			msg = "This field is required."
			self._errors['title'] = self.error_class([msg])

		if not code:
			msg = "This field is required."
			self._errors['code'] = self.error_class([msg])

		if not description:
			msg = "This field is required."
			self._errors['description'] = self.error_class([description])

		return self.cleaned_data