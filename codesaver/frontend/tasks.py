from celery import Celery, task
from django.conf import settings
from boto.ses import SESConnection
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

app = Celery('tasks', broker=settings.BROKER_URL)

@task
def send_code_via_email(profile, email):
	connection = SESConnection(aws_access_key_id=settings.AWS_ACCESS_KEY_ID, aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY)
	print profile
	print email
