from django.conf.urls import patterns, include, url

from frontend.views import IndexView

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', IndexView.as_view(), name='home'),
    url(r'^accounts/', include('accounts.urls')),
    url(r'^frontend/', include('frontend.urls')),

    url(r'^admin/', include(admin.site.urls)),
)
