from django.conf.urls import patterns, include, url
from .views import LoginView, SignupView, SignoutView, ProfileUpdateView

urlpatterns = patterns('',
						url(
							regex=r'^login/$',
							view = LoginView.as_view(),
							name = 'login'
						),
						url(
							regex=r'^signup/$',
							view = SignupView.as_view(),
							name = 'signup'
						),
						url(
							regex=r'^update/$',
							view = ProfileUpdateView.as_view(),
							name = 'profile-update'
						),
						url(
							regex=r'^signout/$',
							view = SignoutView.as_view(),
							name = 'signout'
						),
					)