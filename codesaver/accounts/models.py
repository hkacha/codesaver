from django.db import models
from django.contrib.auth.models import User, AbstractUser
from django.template.defaultfilters import slugify
from django.db.models.signals import post_save

class Profile(AbstractUser):
	"""
	"""
	profile_pic = models.FileField(upload_to='profile_pic', blank=True, null=True)
	date_of_join = models.DateTimeField(auto_now_add=True)
	slug_id = models.SlugField(max_length=200, blank=True, null=True)

	class Meta:
		verbose_name = "Profile"
		verbose_name_plural = "Profiles"

	def __unicode__(self):
		return "%s" % (self.username)

def generate_slugid(instance, created, **kwargs):
	if created:
		instance.slug_id = '%s-%s' % (instance.username, instance.pk)
		instance.save()

post_save.connect(generate_slugid, sender=Profile)