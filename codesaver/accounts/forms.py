from django import forms
from django.contrib import auth
from django.contrib.auth.models import User
from django.contrib.auth import (login, logout, authenticate)

from .models import Profile

class LoginForm(forms.Form):
	"""
	Login Form
	"""
	username = forms.CharField()
	password = forms.CharField(widget=forms.PasswordInput)

	def clean(self):
		cleaned_data = super(LoginForm, self).clean()

		username = cleaned_data.get('username')
		password = cleaned_data.get('password')

		if not username:
			msg = "This field is required."
			self._errors['username'] = self.error_class([msg])

		if not password:
			msg = "This field is required."
			self._errors['password'] = self.error_class([msg])

		if username and password:
			try:
				account = Profile.objects.get(username__iexact=username)
			except:
				msg = "Username or Password Is Incorrect"
				self._errors["password"] = self.error_class([msg])
				return self.cleaned_data

			user = auth.authenticate(username=account.username, password=password)

			if user is None or not user.is_active:
				msg = "Username or Password Is Incorrect"
				self._errors["password"] = self.error_class([msg])
				return self.cleaned_data

		return self.cleaned_data


class SignupForm(forms.ModelForm):
	"""
	Signup Form
	"""

	cnfpassword = forms.CharField(widget=forms.PasswordInput(), label="cnfpassword")

	class Meta:
		model = Profile
		fields = ('username', 'email', 'password')

	def save(self):
		instance = super(SignupForm, self).save()
		username = self.cleaned_data["username"]
		instance.set_password(self.cleaned_data["password"])
		return instance.save()

	def clean(self):
		cleaned_data = super(SignupForm, self).clean()

		email = cleaned_data.get('email')
		password = cleaned_data.get('password')
		cnfpassword = cleaned_data.get('cnfpassword')

		if not email:
			msg = "This field is required."
			self._errors['email'] = self.error_class([msg])

		check_email = Profile.objects.filter(email=email).exists()
		if check_email:
			msg = "User with this Email already exists."
			self._errors['email'] = self.error_class([msg])

		if password != cnfpassword:
			msg = "Password does't match."
			self._errors["cnfpassword"] = self.error_class([msg])

		return self.cleaned_data


class ProfileUpdateForm(forms.ModelForm):
	"""
	Profile Update Form
	"""

	class Meta:
		model = Profile
		fields = ('username', 'first_name', 'last_name', 'email', 'profile_pic')

	def save(self):
		instance = super(ProfileUpdateForm, self).save()
		return instance.save()
    