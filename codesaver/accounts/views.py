from django.contrib.auth import (login, logout, authenticate)
from django.views.generic import TemplateView, FormView, RedirectView, UpdateView
from django.shortcuts import get_object_or_404
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.contrib.auth.models import User
from django.contrib import messages

from accounts.models import Profile
from accounts.forms import SignupForm, LoginForm, ProfileUpdateForm

class LoginView(FormView):
	"""
	docstring for LoginView
	"""
	template_name = "accounts/login.html"
	form_class = LoginForm
	success_url = '/'

	def form_valid(self, form):
		username = form.cleaned_data['username']
		password = form.cleaned_data['password']

		user = authenticate(username=username, password=password)
		if user is not None:
			if user.is_active:
				login(self.request, user)
				return HttpResponseRedirect(self.get_success_url())
			else:
				return self.form_invalid(form)
		else:
			return self.form_invalid(form)
		return HttpResponseRedirect(self.get_success_url())

	def form_invalid(self, form):
		return self.render_to_response(self.get_context_data(form=form))

	def post(self, request, *args, **kwargs):
		form_class = self.get_form_class()
		form = self.get_form(form_class)
		if form.is_valid():
			return self.form_valid(form)
		else:
			return self.form_invalid(form)


class SignupView(FormView):
	"""
	docstring for SignupView
	"""
	template_name = "accounts/signup.html"
	form_class = SignupForm
	success_url = "/"

	def get_context_data(self, **kwargs):
		context = super(SignupView, self).get_context_data(**kwargs)
		return context

	def form_valid(self, form):
		form.save()
		msg = '<strong>Thank you!</strong> Your account has been created successfully. <a href="/accounts/login/" class="alert-link login-link">Click here for login.</a>'
		messages.success(self.request, msg)
		return self.render_to_response(self.get_context_data(form=form))

	def form_invalid(self, form):
		return self.render_to_response(self.get_context_data(form=form))

	def post(self, request, *args, **kwargs):
		form_class = self.get_form_class()
		form = self.get_form(form_class)
		if form.is_valid():
			return self.form_valid(form)
		else:
			return self.form_invalid(form)


class ProfileUpdateView(UpdateView):
	"""
	docstring for ProfileUpdateView
	"""
	template_name = "accounts/profileupdate.html"
	form_class = ProfileUpdateForm
	context_object_name = 'profile'

	def get_object(self):
		return get_object_or_404(Profile, username=self.request.user.username)

	def form_valid(self, form):
		form.save()
		msg = 'Your profile has been updated successfully.'
		messages.success(self.request, msg)
		return self.render_to_response(self.get_context_data(form=form))


class SignoutView(RedirectView):
	"""
	docstring for SignoutView
	"""
	def get_redirect_url(self):
		logout(self.request)
		return reverse('home')
		